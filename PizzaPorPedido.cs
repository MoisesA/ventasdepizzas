ï»¿using System;
using System.Collections.Generic;
//using System.Text;

namespace ventas
{
    class PizzaPorPedido
    {

        private PizzaPorPedido()
        {
            
        }
        private Pizza pizza;

        public Pizza Pizza
        {
            get { return this.pizza; }
            set { this.pizza = value; }
        }
        private Pedido pedido;

        public Pedido Pedido
        {
            get { return this.pedido; }
            set { this.pedido = value; }
        }
        private List<Ingredientes> ingredientes;

        public List<Ingredientes> Ingredientes
        {
            get { return this.ingredientes; }
            set { this.ingredientes = value; }
        }

    }
}
