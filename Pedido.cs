ï»¿using System;
using System.Collections.Generic;
//using System.Text;

namespace ventas
{
    class Pedido:PizzaPorPedido
    {
        public Pedido()
        {

        }
        private String fecha;

        public String Fecha
        {
            get { return this.fecha; }
            set { this.fecha = value; }
        }

        private String numerodeentrega;

        public String NumeroDeEntrega
        {
            get { return this.numerodeentrega; }
            set { this.numerodeentrega = value; }
        }
        private Cliente cliente;

        public Cliente Cliente
        {
            get { return this.cliente; }
            set { this.cliente = value; }
        }
        private List<PizzaPorPedido> pizzasPorPedido;

        public List<PizzaPorPedido> PizzaPorPedidos
        {
            get { return this.pizzasPorPedido; }
            set { this.pizzasPorPedido = value; }
        }


    }
}
